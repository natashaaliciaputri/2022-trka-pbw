<?php include 'header.php';?>
        <h1>Program Studi</h1>
        </div>
        <?php if(!empty($_SESSION['username'])) { ?>
       
        <a href="formProdi.php" class="btn btn-info btn-sm mb-3 mt-3" >Tambah</a>
                 <?php } ?> 

        <table class="table">
        <thead class="table-info">
            <tr>
                <th>ID Prodi</th>
                <th>Nama Prodi</th>

            <?php if(!empty($_SESSION['username'])) { ?>
                <th>Aksi</th>
                <?php } ?> 
            </tr>

        </thead>
        <tbody>
            

            <?php
                $sql = 'SELECT * FROM prodi';
                $query = mysqli_query($conn, $sql);
        
                while ($row = mysqli_fetch_object($query)) {
                    ?>

            <tr>
                <td> <?php echo $row->id_prodi; ?> </td>
                <td><?php echo $row->nama_prodi;?></td>
                <td>

                <?php if(!empty($_SESSION['username'])) { ?>

                    <a href= "formProdi.php?id_prodi=<?php echo $row->id_prodi; ?> "class = "btn btn-sm btn-info"> Ubah</a>
                    <a href="deleteProdi.php?id_prodi=<?php echo $row->id_prodi; ?> "class = "btn btn-info btn-sm" onclick= "return confirm('Hapus data Prodi?');" >Hapus</a>
                    <?php } ?> 
                </td>
            </tr>

            <?php
                 }

                if (! mysqli_num_rows($query)) {
                    echo '<tr><td colspan="8 class="tect-center">Data Tidak ada. </td></tr>';
                }
            ?>
        </tbody>
        </table>

<?php include 'footer.php';?>